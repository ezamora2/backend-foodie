INSERT INTO `restaurant` (`id_user`, `name`, `address`, `rfc`, `type`, `is_approved`) VALUES
    ('1', 'Gorditas los Arcos', 'P.º Loma Nte. 7840, Loma Dorada Delegación B, 45418 Tonalá, Jal.', 'ROVR630406H28', 'Comida rápida', '1'),
    ('1', 'El Parián de Tlaquepaque', 'C. Juárez 68, Centro, 45500 San Pedro Tlaquepaque, Jal.', 'ZAGL701201S32', 'Gourmet', '1'),
    ('1', 'La Plantería Vivero Café', 'Av. Camino Real a Colima 3024, Hacienda del Oro, 45645 San Agustín, Jal.', 'CAME620803803', 'Comida rápida', '0'),
    ('1', 'El Canelo', 'Carr. libre Guadalajara - Zapotlanejo 3852, Tateposco, 45630 Guadalajara, Jal.', 'RARM641007KZ0', 'Temático', '1');

INSERT INTO `meal` (`id_restaurant`, `name`, `description`, `price`, `path_photography`) VALUES
    (1, 'Plato de desayuno', 'Chilaquiles, huevo y café.', 49.00, NULL),
    (1, 'Sandwich de lomo', 'De lomo, pollo, panela o jamón. Con verdura y aderezo.', 25.00, NULL),
    (1, 'Taza de café', '250 ml. de café negro. Perfecto para iniciar tu mañana.', 15.00, NULL),
    (1, 'Guiso del día', 'Plato que incluye el guisado del día, un acomapañante y una bebida.', 50.00, NULL),
    (2, 'Hamburguesa', 'Hamburguesa clásica con carne de res, verdura y aderezos.', 55.00, NULL),
    (2, 'Tacos de adobada', 'Orden de 5 taquitos de adobada.', 30.00, NULL),
    (2, 'Agua de sabor', 'Vaso de agua de sabor del día.', 12.00, NULL),
    (2, 'Jugo natural', 'Jugo natural hecho 100% de frutas de temporada.', 18.00, NULL),
    (2, 'Papas a la francesa', '250 gramos de papas fritas a la francesa, acompañadas de aderezos.', 20.00, NULL),
    (3, 'Fruta', 'Fruta fresca, preparada con limón, sal y chile al gusto.', 18.00, NULL),
    (3, 'Gelatina', 'Vasito de gelatina de mosaico.', 10.00, NULL),
    (3, 'Orden de huevos', 'Huevos revueltos con frijoles.', 35.00, NULL),
    (4, 'Torta', 'De lomo, panela, jamón, hawaiana o cubana. Con verdura  y aderezo.', 40.00, NULL),
    (4, 'Orden de hotcakes', 'Orden de 3 deliciosos hotcakes con mermelada o leche condensada.', 30.00, NULL),
    (4, 'Hot dog', 'Hot dog clásico, con jitomate, cebolla y aderezos.', 25.00, NULL);