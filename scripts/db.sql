DROP DATABASE IF EXISTS foodie;
CREATE DATABASE IF NOT EXISTS foodie;

USE foodie;

-- ----------------------------------------------------
-- tables
-- ----------------------------------------------------

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user`(
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `full_name` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `email` VARCHAR(320) NOT NULL,
    `phone` VARCHAR(14) NOT NULL,
    `birth_date` DATE NOT NULL,
    `password` VARCHAR(20) NOT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci;

DROP TABLE IF EXISTS `restaurant`;
CREATE TABLE IF NOT EXISTS `restaurant`(
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `id_user` INT(10) UNSIGNED NOT NULL,
    `name` VARCHAR(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `address` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `rfc` VARCHAR(13) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `type` VARCHAR(50) NOT NULL,
    `is_approved` TINYINT(2) NOT NULL,
    `deleted_at` DATETIME(0) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci;

DROP TABLE IF EXISTS `meal`;
CREATE TABLE IF NOT EXISTS `meal`(
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `id_restaurant` INT(10) UNSIGNED NOT NULL,
    `name` VARCHAR(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `description` VARCHAR(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `price` DECIMAL(9, 2) NOT NULL,
    `path_photography` VARCHAR(200) NULL DEFAULT NULL,
    `deleted_at` DATETIME(0) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1;