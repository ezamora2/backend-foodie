import express from 'express'
import cors from 'cors'
import morgan from 'morgan'
import usersRoutes from './routes/users'
import restaurantsRoutes from './routes/restaurants'

const app = express()

app.use(cors())

app.use(morgan('dev'))

app.use(express.json())
app.use(express.urlencoded({ extended: false }));

app.use(usersRoutes)
app.use(restaurantsRoutes)

export default app