import { Router } from "express"
import { getRestaurants, getRestaurant, postRestaurant, postMeal, editRestaurant, editMeal, deleteRestaurant, deleteMeal } from "../controllers/restaurants"

const router = Router()

router.get('/restaurants', getRestaurants)
router.get('/restaurants/:id/meals', getRestaurant)
router.post('/restaurants', postRestaurant)
router.post('/restaurants/:id/meals', postMeal)
router.put('/restaurants/:id', editRestaurant)
router.put('/restaurants/:id/meals/:id_meal', editMeal)
router.delete('/restaurants/:id', deleteRestaurant)
router.delete('/restaurants/:id/meals/:id_meal', deleteMeal)

export default router