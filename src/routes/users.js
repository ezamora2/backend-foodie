import { Router } from "express"
import { getUser, getUserRestaurants, signIn, signUp } from "../controllers/users"

const router = Router()

router.get('/users/:id', getUser)
router.get('/users/:id/restaurants', getUserRestaurants)
router.post('/users/signup', signUp)
router.post('/users/signin', signIn)

export default router