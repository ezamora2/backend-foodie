import {connect} from '../database'

export const getRestaurants = async (req, res) => {
    const connection = await connect();
    const queryRestaurants = 'SELECT * FROM `restaurant`;';
    let [restaurants] = await connection.query(queryRestaurants).catch(function (err) {
        console.log("Promise Rejected" + err)
    })
    for (const [keyRestaurant, restaurant] of restaurants.entries()) {
        restaurant['meals'] = await _getRestaurantMeals(connection, restaurant.id)
    }
    await connection.end()
    res.json(restaurants)
}

export const getRestaurant = async (req, res) => {
    const connection = await connect();
    const queryRestaurant = 'SELECT * FROM `restaurant` WHERE id = ?;';
    let [restaurant] = await connection.query(queryRestaurant, [req.params.id]).catch(function (err) {
        console.log("Promise Rejected" + err)
    })
    restaurant = restaurant[0]
    restaurant['meals'] = await _getRestaurantMeals(connection, restaurant.id)
    await connection.end()
    res.json(restaurant)
}

export const postRestaurant = async (req, res) => {
    const connection = await connect();
    const queryRestaurant = 'INSERT INTO `restaurant` (`id_user`, `name`, `address`, `rfc`, `type`, `is_approved`) VALUES (?, ?, ?, ?, ?, ?);';
    const body = [req.body.id_user, req.body.name, req.body.address, req.body.rfc, req.body.type, 0]
    let [restaurant] = await connection.query(queryRestaurant, body).catch(function (err) {
        console.log("Promise Rejected" + err)
    })
    res.json({
        id_restaurant: restaurant.insertId,
        ...req.body
    })
}

export const postMeal = async (req, res) => {
    res.send("This endpoint hasn't been finished yet, but it'll be available soon!")
}

export const editRestaurant = async (req, res) => {
    res.send("This endpoint hasn't been finished yet, but it'll be available soon!")
}

export const editMeal = async (req, res) => {
    res.send("This endpoint hasn't been finished yet, but it'll be available soon!")
}

export const deleteRestaurant = async (req, res) => {
    res.send("This endpoint hasn't been finished yet, but it'll be available soon!")
}

export const deleteMeal = async (req, res) => {
    res.send("This endpoint hasn't been finished yet, but it'll be available soon!")
}

const _getRestaurantMeals = async (connection, id_restaurant) => {
    let query = "SELECT * FROM `meal` WHERE id_restaurant = ?;"
    const [meals] = await connection.query(query, [id_restaurant])
    return meals;
}