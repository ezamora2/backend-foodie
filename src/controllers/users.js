import {connect} from '../database'

export const getUser = async (req, res) => {
    const connection = await connect();
    const queryUser = 'SELECT * FROM `user` WHERE id = ?;';
    let [rowUser] = await connection.query(queryUser, [req.params.id]).catch(function (err) {
        console.log("Promise Rejected" + err)
    })
    await connection.end()
    res.json(rowUser)
}

export const getUserRestaurants = async (req, res) => {
    const connection = await connect();
    const queryUserRestaurants = 'SELECT * FROM `restaurant` WHERE id_user = ?;';
    let [userRestaurants] = await connection.query(queryUserRestaurants, [req.params.id]).catch(function (err) {
        console.log("Promise Rejected" + err)
    })
    await connection.end()
    res.json(userRestaurants)
}

export const signUp = async (req, res) => {
    const connection = await connect();
    const error = _validateParamsSignup(req.body)
    if (error.length === 0) {
        if (await _isEmailUnique(connection, req.body.email)) {
            const querySignUp = 'INSERT INTO `user` (`full_name`, `email`, `phone`, `birth_date`, `password`) VALUES (?, ?, ?, ?, ?);';
            const body = [req.body.full_name, req.body.email, req.body.phone, req.body.birth_date, req.body.password]
            let [signUp] = await connection.query(querySignUp, body).catch(function (err) {
                console.log("Promise Rejected" + err)
            })
            await connection.end()
            res.json({
                id_user: signUp.insertId,
                ...req.body
            })
        } else {
            await connection.end()
            res.send([{
                error_code: "signuperror7",
                error: "Email already exists"
            }])
        }
    } else {
        await connection.end()
        res.send(error)
    }
}

export const signIn = async (req, res) => {
    const connection = await connect();
    const querySignIn = 'SELECT * FROM `user` WHERE email = ? AND password = ?;';
    let [signIn] = await connection.query(querySignIn, [req.body.email, req.body.password]).catch(function (err) {
        console.log("Promise Rejected" + err)
    })
    await connection.end()
    res.json(signIn)
}

function _validateParamsSignup(params) {
    let errorlog = []
    const regexName = /^[a-zA-ZÑñÁáÉéÍíÓóÚúÜü\s]+$/
    const regexEmail = /^\w+@(\w+\.)+\w{2,4}$/
    const regexPhone = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/
    const regexPassword = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/
    const regexDate = /^\d{4}-\d{2}-\d{2}$/

    if (!regexName.exec(params.full_name)) {
        errorlog.push({
            error_code: "signuperror1",
            error: "Invalid full name"
        })
    }
    if (!regexEmail.exec(params.email)) {
        errorlog.push({
            error_code: "signuperror3",
            error: "Invalid email"
        })
    }
    if (!regexPhone.exec(params.phone)) {
        errorlog.push({
            error_code: "signuperror4",
            error: "Invalid phone"
        })
    }
    if (!regexPassword.exec(params.password)) {
        errorlog.push({
            error_code: "signuperror5",
            error: "Invalid password. The password must contain at least 8 and not more than 20 characters, at least one uppercase letter, one lower case letter and one number."
        })
    }
    if (!regexDate.exec(params.birth_date)) {
        errorlog.push({
            error_code: "signuperror6",
            error: "Invalid birth_date. Dates must be in format \'yyyy-mm-dd\'."
        })
    }

    return errorlog
}

const _isEmailUnique = async (connection, email) => {
    let query = "SELECT COUNT(*) FROM user WHERE email = ?;"
    const [existing_emails] = await connection.query(query, [email])
    console.log(existing_emails[0]['COUNT(*)'])
    if (existing_emails[0]['COUNT(*)'] === 0) {
        return true
    } else {
        return false
    }
}