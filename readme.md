# Set up

## Clone de project

> git clone git@gitlab.com:ezamora2/foodie.git

## Download npm dependencies

> npm install

## Create .env file in the root directory and set the variables

```
DB_HOST = 
DB_PORT = 
DB_USER = 
DB_PASSWORD = 
DB_NAME = 
```

## Start API
> npm run dev